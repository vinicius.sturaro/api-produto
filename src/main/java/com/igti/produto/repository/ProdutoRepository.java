package com.igti.produto.repository;

import com.igti.produto.entity.Produto;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface ProdutoRepository extends MongoRepository<Produto, String> {
    
    Optional<Produto> findByNomeIgnoreCase(String nome);
}
