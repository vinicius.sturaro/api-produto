package com.igti.produto.controller;

import com.igti.produto.dto.ProdutoDto;
import com.igti.produto.entity.Produto;
import com.igti.produto.repository.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/produto")
public class ProdutoController {
    
    @Autowired
    ProdutoRepository produtoRepository;
    
    @GetMapping
    public ResponseEntity<List<Produto>> listar() {
        List<Produto> produtos = produtoRepository.findAll();
        return new ResponseEntity<>(produtos, HttpStatus.OK);
    }
    
    @GetMapping(path = "{id}")
    public ResponseEntity<Produto> buscarPorId(@PathVariable("id") String id) {
        Optional<Produto> produto = produtoRepository.findById(id);
        if(!produto.isPresent())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            
        return new ResponseEntity<>(produto.get(), HttpStatus.OK);
    }
    
    @GetMapping(path = "/nome/{nome}")
    public ResponseEntity<Produto> buscarPorNome(@PathVariable("nome") String nome) {
        Optional<Produto> produto = produtoRepository.findByNomeIgnoreCase(nome);
        if(!produto.isPresent())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(produto.get(), HttpStatus.OK);
    }
    
    @DeleteMapping(path = "{id}")
    public ResponseEntity<Produto> deletar(@PathVariable("id") String id) {
        Optional<Produto> produto = produtoRepository.findById(id);
        if(!produto.isPresent())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        
        produtoRepository.delete(produto.get());
        
        return new ResponseEntity<>(HttpStatus.OK);
    }
    
    @PostMapping
    public ResponseEntity<Void> criar(@RequestBody @Valid ProdutoDto produtoDto) {
        Produto produto = new Produto(produtoDto);
        produtoRepository.save(produto);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
    
    @PutMapping(path = "{id}")
    public ResponseEntity<Void> atualizar(@PathVariable("id") String id, @RequestBody @Valid ProdutoDto produtoDto) {
        Optional<Produto> produto = produtoRepository.findById(id);
        if(!produto.isPresent())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        
        produto.get().atualizarValores(produtoDto);
        produtoRepository.save(produto.get());
        
        return new ResponseEntity<>(HttpStatus.OK);
    }
    
    @PatchMapping(path = "{id}")
    public ResponseEntity<Void> atualizarAtributo(@PathVariable("id") String id, @RequestBody @Valid ProdutoDto produtoDto) {
        Optional<Produto> produto = produtoRepository.findById(id);
        if(!produto.isPresent())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        
        produto.get().atualizarAtributo(produtoDto);
        produtoRepository.save(produto.get());
        
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
