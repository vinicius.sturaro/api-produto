package com.igti.produto.entity;

import com.igti.produto.dto.ProdutoDto;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;

@Data
@Document
public class Produto {
    
    @Id
    private String id;
    private String nome;
    private String descricao;
    private String categoria;
    private Integer quantidade;
    private BigDecimal valor;
    
    public Produto() {
    }
    
    public Produto(ProdutoDto produtoDto) {
        this.nome = produtoDto.getNome();
        this.descricao = produtoDto.getDescricao();
        this.categoria = produtoDto.getCategoria();
        this.quantidade = produtoDto.getQuantidade();
        this.valor = produtoDto.getValor();
    }
    
    public void atualizarValores(ProdutoDto produtoDto) {
        this.nome = produtoDto.getNome();
        this.descricao = produtoDto.getDescricao();
        this.categoria = produtoDto.getCategoria();
        this.quantidade = produtoDto.getQuantidade();
        this.valor = produtoDto.getValor();
    }
    
    public void atualizarAtributo(ProdutoDto produtoDto) {
        this.nome = produtoDto.getNome() != null ? produtoDto.getNome() : this.nome;
        this.descricao = produtoDto.getDescricao() != null ? produtoDto.getDescricao() : this.descricao;
        this.categoria = produtoDto.getCategoria() != null ? produtoDto.getCategoria() : this.categoria;
        this.quantidade = produtoDto.getQuantidade() != null ? produtoDto.getQuantidade() : this.quantidade;
        this.valor = produtoDto.getValor() != null ? produtoDto.getValor() : this.valor;
    }
}
