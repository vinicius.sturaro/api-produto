package com.igti.produto.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ProdutoDto {
    private String nome;
    private String descricao;
    private String categoria;
    private Integer quantidade;
    private BigDecimal valor;
}
